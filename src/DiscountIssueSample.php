<?php

namespace dlouhy\IsicVerification;

/**
 * This script represents basic discount issuing process. Each step is described
 * in detail in the inline comments. Specifically this script will perform
 * following actions:
 *
 * 1. Verify card validity and evaluate eligibility.
 * 2. Create receipt for the issued discount.
 * 3. Revoke created receipt.
 *
 * Basic usage:
 * * Change constants on the top of the class, if necessary.
 * * Save all changes.
 * * Run the script. (e.g. 'php DiscountIssueSample.php')
 * * Results will be printed to the console.
 */
class DiscountIssueSample {

    // Discount manager instance URL (testing environment by default).
    const DM_URL = "https://gts-dm.orchitech.net";

    // Valid credentials for testing environment - please obtain yours from GTS Alive.
    const DM_PROVIDER_USERNAME = "testdm";
    const DM_PROVIDER_PASSWORD = "testdm";

    // Information about the card to be verified.
    const CARD_CARD_NUMBER = "S123456789000A"; // Card number as printed on the card.
    const CARD_CARDHOLDER_NAME = "John Doe"; // Name of the cardholder as printed on the card.

    // Identifier of the discount being issued.
    /*
     * Discount is represented by an internal numeric identifier of the discount within Discount Manager.
     * Note that it has to be benefit of the same provider as user's, because providers can only issue
     * their own benefits.
     */
    const DISCOUNT_ID = 156;

    /**
     * Run the sample.
     */
    public static function run() {

        /*
         * We will create an instance of the discount manager client abstracting WS calls to Discount Manager.
         * It internally makes requests to the JSON REST API. For more information refer to its implementation
         * and official integration manual.
         */
        $discountManagerClient = new DiscountManagerClient(self::DM_URL, self::DM_PROVIDER_USERNAME, self::DM_PROVIDER_PASSWORD);

        /*
         * Imagine that user has selected a product in your (online) store and wants to apply for an ISIC discount.
         * We know that the discount is listed in the discount manager under {DISCOUNT_ID}, which is something
         * that should probably be part of the configuration.
         *
         * First of all we have to make sure that the user is allowed to redeem the discount, which means
         * that they have a valid ISIC card and fulfill all restrictions specified in Discount manager.
         * Both can be done using the call shown below.
         *
         * (Normally the user would enter card details into the form, but we will use the values from configuration for simplicity.)
         */
        echo "Verifying card '" . self::CARD_CARD_NUMBER . "' of customer '" . self::CARD_CARDHOLDER_NAME .
            "'. For discount '" .self::DISCOUNT_ID . "'." . PHP_EOL;
        $verificationResult = $discountManagerClient->verifyCard(self::CARD_CARD_NUMBER, self::CARD_CARDHOLDER_NAME, self::DISCOUNT_ID);

        // Store the verification identifier for future usage.
        $verificationId = $verificationResult['id'];
        echo "Created verification with ID '" . $verificationId . "'." .PHP_EOL;

        // You can print the result of verification request if you want by uncommenting the line below.
        //echo "Result of card verification:" . PHP_EOL . print_r($verificationResult) . PHP_EOL;

        /*
         * In this example we don't process errors of the HTTP request itself (wrong URL, invalid credentials,
         * missing parameters, etc.). For those cases the client will simply throw and exception and you should
         * check your configuration.
         *
         * However, we have to inspect result of the verification when the request is successful.
         */

        // Check the result of basic card verification.
        if ($verificationResult['result'] == DiscountManagerClient::VERIFICATION_RESULT_FAILED) {
            // Card verification failed.
            echo "Card is invalid - The discount cannot be issued." . PHP_EOL;
            // We can further check the reason why was the verification unsuccessful.
            switch ($verificationResult['reason']) {
                case 'INVALID_NUMBER':
                    echo "Card number does not have a valid format." . PHP_EOL;
                    return;
                case 'CARD_NOT_FOUND':
                    echo "Card with the specified number has not been found." . PHP_EOL;
                    return;
                case 'CARD_CANCELLED':
                    echo "Card with the specified number has been cancelled" . PHP_EOL;
                    return;
                case 'CARD_EXPIRED':
                    echo "Card with the specified number is already expired" . PHP_EOL;
                    return;
                case 'CARD_VALID_IN_FUTURE':
                    echo "Card with the specified number is not yet valid". PHP_EOL;
                    return;
                case 'NAME_MISMATCH':
                    echo "Provided cardholder name does not match name printed on the card". PHP_EOL;
                    return;
                case 'OTHER':
                    echo "Other unspecified verification failure". PHP_EOL;
            }
            return;

        } else if ($verificationResult['result'] == DiscountManagerClient::VERIFICATION_RESULT_SUCCESSFUL) {
            // The card is valid. We can additionally check the eligibility if we want.
            if (!empty($verificationResult['eligibilityErrors'])) {
                // In case of any eligibility errors the user should not be able to get the discount.
                // Lookup the eligibility errors in the integration manual.
                echo "Cardholder is not eligible to get the discount. Reasons:" . PHP_EOL;
                echo print_r($verificationResult['eligibilityErrors'], true) . PHP_EOL;
                return;
            }
            echo "Card is successfully verified and cardholder is eligible to receive the discount." . PHP_EOL;
        } else {
            // This should never happen.
            throw new Exception("Invalid verification result - this should never happen.");
        }

        /*
         * Now, when we know that the user is eligible to receive the discount, we can proceed to the payment, etc.
         * When the order is fully processed and the user receives the service/product, we should let the
         * Discount Manager know that the benefit has been successfully issued. We should provide ID of the
         * verification that allowed issuing of the discount and exact date of issue. Current date is
         * sufficient for the purpose of this sample (and probably most of other cases).
         *
         * It can be done simply using following call.
         */
        echo "Creating receipt for discount '" . self::DISCOUNT_ID . "' verified by verification '" . $verificationId . "'." . PHP_EOL;
        $receiptCreationResult = $discountManagerClient->createReceipt(self::DISCOUNT_ID, $verificationId, new DateTime());

        // Store receipt ID for future usage.
        $receiptId = $receiptCreationResult['id'];
        echo "Created receipt with id: '" . $receiptId . "'." .PHP_EOL;

        // You can print the result of receipt creation if you want by uncommenting the line below.
        // echo "Result of the receipt creation:" . PHP_EOL . print_r($receiptCreationResult) . PHP_EOL;

        /*
         * At this point we are normally finished. The discount has been successfully issued and you should see
         * both the verification and the receipt in the administration of the Discount Manager.
         *
         * However, it might happen that the user will cancel their order for instance. In this case we have
         * 30 days to revoke the receipt. We just need to remember the ID of the receipt.
         */
        echo "Revoking receipt '" . $receiptId . "'." . PHP_EOL;
        $discountManagerClient->revokeReceipt($receiptId);
        echo "Receipt '" . $receiptId . "' revoked." . PHP_EOL;

    }

}

// Run the sample issuing process.
DiscountIssueSample::run();
