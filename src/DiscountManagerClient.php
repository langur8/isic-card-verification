<?php

namespace dlouhy\IsicVerification;

use Symfony\Bridge\Monolog\Logger;

/**
 * Discount Manager client implementation supporting
 * card verification and creation of discount receipt
 * and revoking it.
 */
class DiscountManagerClient {

    const HTTP_METHOD_POST = 'POST';
    const HTTP_METHOD_PUT = 'PUT';

    const VERIFICATION_RESULT_SUCCESSFUL = 'SUCCESSFUL';
    const VERIFICATION_RESULT_FAILED = 'FAILED';

    /**
     * @var string URL of the Discount Manager instance.
     */
    private $url;

    /**
     * @var string Discount manager username.
     */
    private $username;

    /**
     * @var string Discount manager password.
     */
    private $password;

	/**
	 * @var Logger
	 */
	private $logger;

    /**
     * Create new instance of Discount Manager client.
     *
     * @param string $dmUrl Discount Manager URL. Never empty, never null.
     * @param string $dmUsername Discount Manager username. Never empty, never null.
     * @param string $dmPassword Discount Manager password. Never empty, never null.
     */
    public function __construct($dmUrl, $dmUsername, $dmPassword, Logger $logger) {
        $this->url = $dmUrl . "/api";
        $this->username = $dmUsername;
        $this->password = $dmPassword;
        $this->logger = $logger;
    }

    /**
     * Verify card.
     *
     * Sample response:
     * [
     *     "id" => 41,
     *     "createdOn" => "2014-09-18T11:00:00+0100",
     *     "cardNumber" => "S111 222 333 444X",
     *     "cardholderName" => "Peter Parker",
     *     "discountId" => 12,
     *     "result" => "SUCCESSFUL",
     *     "reason" => null,
     *     "cardType" => "IYTC",
     *     "validFrom" => "2013-09-01",
     *     "validTo" => "2014-11-30",
     *     "eligibilityErrors": [
     *     	   [
     *     			"code" => "cardType.invalidValue",
     *              "params" =>  [
     *              	"allowed": [ "ISIC", "ITIC"]
     *         ],
     *         [
     *             "code" => "timeOfDay.outOfRange",
     *	           "params": [ "min" => "06:00:00", "max" => "10:00:00" ]
     *         ]
     *     ]
     * ]
     *
     *
     * @param string $cardNumber Card number. Never empty, never null.
     * @param unknown $cardholderName Name as printed on the card. Never empty, never null.
     * @param string $discountId Optional discount identifier. Can be null.
     *
     * @return array Array representation of the response in case of success. Throws exception otherwise.
     * @throws DiscountManagerClientException in case of problems with card verification.
     */
    public function verifyCard($cardNumber, $cardholderName, $discountId = null) {
        $request = [
            'cardNumber' => $cardNumber,
            'cardholderName' => $cardholderName,
        ];
        if ($discountId != null) {
            $request['discountId'] = $discountId;
        }
        $response = $this->makeRequest(self::HTTP_METHOD_POST, '/verifications', $request);
        return $response;
    }

    /**
     * Create discount usage receipt.
     *
     * Sample response:
     * [
     *     "id" => 12345,
     *     "discountId" => 12345,
     *     "verificationId" => 12345,
     *     "issuedOn" => "2014-09-18T00:00:00+0000",
     *     "createdOn" => "2014-09-18T13:12:46+0000",
     *     "amountPaid" => 152.12
     * ]
     *
     * @param integer $discountId Issued discount identifier. Never null.
     * @param integer $verificationId Identifier of the verification leading to issuing of the discount. Never null.
     * @param \DateTime $issuedOn Benefit issuing date. Never null.
     * @param string $amountPaid Amount paid by the customer for the issued service. Never null.
     *
     * @return array Receipt creation response in case of success. Throws exception otherwise.
     * @throws DiscountManagerClientException in case of problems with card verification.
     *
     */
    public function createReceipt($discountId, $verificationId, \DateTime $issuedOn, $amountPaid = null) {
        $request = [
            "discountId" => $discountId,
            "verificationId" => $verificationId,
            "issuedOn" => $issuedOn->format(\DateTime::ISO8601),
        ];
        if ($amountPaid !== null) {
            $request["amountPaid"] = $amountPaid;
        }
        $response = $this->makeRequest(self::HTTP_METHOD_POST, "/receipts", $request);
        return $response;
    }

    /**
     * Revoke receipt of the issued discount.
     *
     * @param integer $receiptId Receipt identifier. Never null.
     * @return boolean True in case of success. Throws exception otherwise.
     * @throws DiscountManagerClientException in case of problems with card verification.
     */
    public function revokeReceipt($receiptId) {
        $request = [
            'status' => 'REVOKED'
        ];
        return $this->makeRequest(self::HTTP_METHOD_PUT, "/receipts/" . $receiptId . "/status", $request);
    }

    /**
     * Make JSON request to Discount manager.
     *
     * @param string $method HTTP method to be used.
     * @param string $path Path to the resource within DM api.
     * @param array $data Array represenation of the data.
     */
    private function makeRequest($method, $path, $data) {

        // Encode request.
        $requestBody = json_encode($data);
        // Assemble request.
        $process = curl_init($this->url . $path);
        curl_setopt($process, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Encoding: UTF-8'));
        curl_setopt($process, CURLOPT_HEADER, 0);
        curl_setopt($process, CURLOPT_USERPWD, $this->username . ":" . $this->password);
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($process, CURLOPT_POSTFIELDS, $requestBody);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($process, CURLOPT_SSL_VERIFYPEER, FALSE);

        // Process the request.
        $return = curl_exec($process);
        $requestInfo = curl_getinfo($process);
        curl_close($process);
        // Resolve response status to more descriptive messages.
        $statusCode = $requestInfo['http_code'];
        if ($statusCode === 200 || $statusCode === 201) {
			$this->logger->debug('return: ' . $return . '; info:' . serialize($requestInfo));
            return json_decode($return, true);
        } else if ($statusCode === 400) {
			$this->logger->error('return: ' . $return . '; info:' . serialize($requestInfo));
            throw new DiscountManagerClientException("Neplatné parametry poždavku.", $statusCode);
        } else if ($statusCode === 404) {
			$this->logger->error('return: ' . $return . '; info:' . serialize($requestInfo));
            throw new DiscountManagerClientException("Neplatná URL služby", $statusCode);
        } else if ($statusCode === 401) {
			$this->logger->error('return: ' . $return . '; info:' . serialize($requestInfo));
            throw new DiscountManagerClientException("Neplatné přihlašovací údaje.", $statusCode);
        } else  if ($statusCode === 403) {
			$this->logger->error('return: ' . $return . '; info:' . serialize($requestInfo));
            throw new DiscountManagerClientException("Uživatel nemá pro danou operaci oprávnění.", $statusCode);
        } else {
			$this->logger->error('return: ' . $return . '; info:' . serialize($requestInfo));
            throw new DiscountManagerClientException("Neočekávaná chyba komunikace.", $statusCode);
        }

    }
}
