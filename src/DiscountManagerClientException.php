<?php

namespace dlouhy\IsicVerification;

/**
 * This exception is thrown in case of any problems while contacting the DM.
 */
class DiscountManagerClientException extends \Exception {

    /**
     * @var array Machine readable errors.
     */
    private $errors;

    /**
     * Create new instance of the exception.
     *
     * @param string $message Exception message.
     * @param number $code HTTP status code.
     * @param array $errors Machine readable represenation of the errors. Can be null.
     * @param Exception $previous Previous exception leading to the error (if any).
     */
    public function __construct($message, $code, $errors = null, \Exception $previous = null) {
        $this->errors = $errors;
        parent::__construct($message, $code, $previous);
    }

    /**
     * Get machine readable validation errors. Can be null.
     *
     * @return array Machine readable representation of validation errors. Can be null.
     */
    public function getErrors() {
        return $this->errors;
    }


    /* (non-PHPdoc)
     * @see Exception::__toString()
     */
    public function __toString() {
        $string = "Exception while contacting DM. Status code: '" . $this->getCode() . "'. " . PHP_EOL;
        $string .= $this->getMessage();
        $errors = $this->getErrors();
        if ($errors !== null) {
            $string .= print_r($errors, true);
        }
        return $string;

    }

}
